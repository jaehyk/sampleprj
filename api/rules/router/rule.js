const express = require('express');
const router = express.Router();
const ruleController = require('../controller/ruleController');

router.get('/', (req, res) => {
	ruleController.findAll({})
		.then(data => res.send(data))
		.catch(err => res.status(500).send(err));
});
router.post('/', (req, res) => {
	ruleController.create(req.body)
		.then(data => res.send(data))
		.catch(err => res.status(500).send(err));
});
router.put('/:rule_id', (req, res) => {
	ruleController.update(req.params.rule_id, req.body)
		.then(data => res.send(data))
		.catch(err => res.status(500).send(err));
});

module.exports = router;
