const Rule = require("../model/Rule");

exports.create = (data) => {
	const rule = new Rule(data);
//	return rule.save();
	return rule.collection.insertMany(data, { ordered : false });
};

exports.findAll = () => {
	return Rule.find({});
};

exports.update = (id, data) => {
	return Rule.findOneAndUpdate({_id: id}, data, {new: true});
};

exports.delete = (id) => {
	return Rule.remove({_id: id});
};
