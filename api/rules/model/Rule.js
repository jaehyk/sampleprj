var mongoose = require("mongoose");

var ruleSchema = mongoose.Schema({
	make: {
		type: String
	},
	model: {
		type: String
	},
	price: {
		type: String
	},
	zombies: {
		type: String
	},
	style: {
		type: String
	},
	clothes: {
		type: String
	},
	useYn: {
		type: String
	},
	date: {
		type: String
	}
});

var Rule = mongoose.model("Rule", ruleSchema);
module.exports = Rule;

