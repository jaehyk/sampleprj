var express = require("express");
var bodyParser = require("body-parser");
var fs = require("fs");
var mongoose = require("mongoose");
var config = require("./config/db");
var app = express();

mongoose.set("useCreateIndex", true);
mongoose
	.connect(config.database, { useNewUrlParser: true })
	.then(() => {
		console.log("connected");
	})
	.catch(err => {
		console.log(err);
	});

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.set("view engine", "ejs");
app.engine("html", require("ejs").renderFile);

app.get("/", function(req, res) {
	res.render("index.html");
});

app.use("/api/v1/rules", require("./api/rules/router/rule"));
app.use(express.static('public'));

/*
app.get("/api/v1/rules", (req, res, next) => {
	fs.readFile("data.json", "utf8", function(err, data) {
		res.json(JSON.parse(data));
	});
});
*/

app.listen(3000, () => {
		console.log("server running on port 3000");
});
